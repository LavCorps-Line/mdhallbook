-- /src/MANIFEST.dhall
-- do not rename me!

-- Imports --

let Prelude = ./lib/prelude.dhall
let Utils = ./lib/utils.dhall
let Module = ./lib/module.dhall

-- Manifest Defs --

let Name : Text = "Example"

let Manifest : Module.Module =
    { id = Utils.Sluginator Name
    , title = Name
    , description = "This is an example module."
    , version = "0.1.0"
    , authors =
        [ { name = "LavCorps-Line"
          , discord = None Text
          , email = None Text
          , url = Some "https://gitlab.com/LavCorps-Line/"
          }
        ]
    , compatibility =
        { minimum = "11"
        , verified = "11"
        , maximum = Some "11"
        }
    , download = Some "${env:CI_PAGES_URL as Text}/${Utils.Sluginator Name}.zip"
    , manifest = Some "${env:CI_PAGES_URL as Text}/module.json"
    , url = Some "${env:CI_PAGES_URL as Text}"
    , packs = [] : List Module.Pack
    }

-- Output --

in { Manifest }
