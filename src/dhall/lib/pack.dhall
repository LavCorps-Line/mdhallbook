-- src/lib/pack.dhall

-- Imports --

let Prelude = ./prelude.dhall
let Utils = ./utils.dhall

-- Type Defs --

let TODO : Type =
    { todo : Text
    }

-- Output --

in  { TODO
    }
