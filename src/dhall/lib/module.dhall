-- src/lib/module.dhall
-- TODO: pack folders support

-- Imports --

let Prelude = ./prelude.dhall
let Utils = ./utils.dhall

-- Type Defs --

let Author : Type =
    { name : Text
    , discord : Optional Text
    , email : Optional Text
    , url : Optional Text
    }

let Compatibility : Type =
    { minimum : Text
    , verified : Text
    , maximum : Optional Text
    }

let Pack : Type =
    { name : Text
    , label : Text
    , path : Text
    , type : < actor | card-stack | item | journal-entry | macro | playlist | rollable-table | scene | adventure >
    , system : < pf2e >
    }

let Module : Type =
    { id : Text
    , title : Text
    , description : Text
    , version : Text
    , authors : List Author
    , compatibility : Compatibility
    , packs : List Pack
    , manifest : Optional Text
    , download : Optional Text
    , url : Optional Text
    }

-- Output --

in  { Author
    , Compatibility
    , Pack
    , Module
    }
