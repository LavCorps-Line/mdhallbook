-- src/lib/utils.dhall

-- Imports --

let Prelude = ./prelude.dhall

-- Functions --

let Exists = \(type : Type) -> \(smth : Optional type) ->
    merge
        { Some = \(_ : type) -> True
        , None = False
        } smth

let Map
    : forall(a : Type) -> forall(b : Type) -> (a -> b) -> List a -> List b
    = \(a : Type) ->
      \(b : Type) ->
      \(f : a -> b) ->
      \(xs : List a) ->
        List/build
          b
          ( \(list : Type) ->
            \(cons : b -> list -> list) ->
              List/fold a xs list (\(x : a) -> cons (f x))
          )

let Mono = \(input : Text) ->
    Text/replace "``" "" ("`${input}`")

let Sluginator
    : Text -> Text
    = List/fold
        (Text -> Text)
        [ Text/replace "A" "a"
        , Text/replace "B" "b"
        , Text/replace "C" "c"
        , Text/replace "D" "d"
        , Text/replace "E" "e"
        , Text/replace "F" "f"
        , Text/replace "G" "g"
        , Text/replace "H" "h"
        , Text/replace "I" "i"
        , Text/replace "J" "j"
        , Text/replace "K" "k"
        , Text/replace "L" "l"
        , Text/replace "M" "m"
        , Text/replace "N" "n"
        , Text/replace "O" "o"
        , Text/replace "P" "p"
        , Text/replace "Q" "q"
        , Text/replace "R" "r"
        , Text/replace "S" "s"
        , Text/replace "T" "t"
        , Text/replace "U" "u"
        , Text/replace "V" "v"
        , Text/replace "W" "w"
        , Text/replace "X" "x"
        , Text/replace "Y" "y"
        , Text/replace "Z" "z"
        , Text/replace " " "-"
        , Text/replace "'" ""
        , Text/replace "." ""
        , Text/replace "(" ""
        , Text/replace ")" ""
        , Text/replace "/" ""
        , Text/replace "!" ""
        ]
        Text
        (\(replacement : Text -> Text) -> replacement)

let Tooltipify = \(name : Text) -> \(tooltip : Text) ->
    let out = "[${Mono name}](# \"${tooltip}\")"
    in out

in  { Exists
    , Map
    , Mono
    , Sluginator
    , Tooltipify
    }
